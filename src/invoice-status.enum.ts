export enum InvoiceStatus {
    REQUEST = 'REQUEST',
    SUCCESSFUL = 'SUCCESSFUL',
    FAILED = 'FAILED'
}

export default InvoiceStatus;