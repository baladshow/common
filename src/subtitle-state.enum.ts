export enum SubtitleState {
    NEW = 'NEW',
    TRANSLATING = 'TRANSLATING',
    UNDER_REVIEW = 'UNDER_REVIEW',
    CANCELED = 'CANCELED',
    VERIFIED = 'VERIFIED',
    REJECTED = 'REJECTED'
}
export default SubtitleState;