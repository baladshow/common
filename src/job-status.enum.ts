export enum JobStatus {
    WAITING = 'WAITING',
    RUNNING = 'RUNNING',
    DONE = 'DONE',
    ERROR = 'ERROR',
    STALLED = 'STALLED'
}

export default JobStatus