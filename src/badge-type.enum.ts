export const enum BadgeType {
    GOLD = 'GOLD',
    SILVER = 'SILVER',
    BRONZE = 'BRONZE'
}
export default BadgeType;