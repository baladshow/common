export enum DeviceType {
    MOBILE = 'mobile',
    DESKTOP = 'desktop',
    TABLET = 'tablet',
    TV = 'tv'
}

export default DeviceType;