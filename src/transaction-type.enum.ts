export enum TransactionType {
    PURCHASE = 'PURCHASE',
    CHARGE = 'CHARGE',
    INCOME = 'INCOME',
    SETTLEMENT = 'SETTLEMENT',
    REVERT = 'REVERT',

}

export default TransactionType;