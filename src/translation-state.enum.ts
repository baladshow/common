export enum TranslationState {
    NEW = 'NEW',
    IN_PROCESS = 'IN_PROCESS',
    UNDER_REVIEW = 'UNDER_REVIEW',
    EXPIRED = 'EXPIRED',
    VERIFIED = 'VERIFIED',
    REJECTED = 'REJECTED',
    CANCELED = 'CANCELED',
}
export default TranslationState;