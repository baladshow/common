export const enum SubscriberMessageType {
    A = 'A',
    B1 = 'B1',
    B2 = 'B2',
    C1 = 'C1',
    C2 = 'C2',
    C3 = 'C3',
    C4 = 'C4',
}
export default SubscriberMessageType;