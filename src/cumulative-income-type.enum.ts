export const enum CumulativeIncomeType {
    DAILY = 'DAILY',
    MONTHLY = 'MONTHLY',
    WEEKLY = 'WEEKLY'
}
export default CumulativeIncomeType;