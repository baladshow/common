export enum ReviewStatus {
    WAITING = 'WAITING',
    ACCEPTED = 'ACCEPTED',
    REJECTED = 'REJECTED',
    DELETED = 'DELETED'
}

export default ReviewStatus;