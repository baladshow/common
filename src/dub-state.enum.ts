export enum DubState {
    NEW = 'NEW',
    DUBBING = 'DUBBING',
    UNDER_REVIEW = 'UNDER_REVIEW',
    CANCELED = 'CANCELED',
    VERIFIED = 'VERIFIED',
    REJECTED = 'REJECTED'
}
export default DubState;
