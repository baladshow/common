export enum Language {
    fa = 'fa',
    en = 'en',
    ar = 'ar',
    ru = 'ru'
}
export default Language;