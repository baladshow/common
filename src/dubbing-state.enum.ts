export enum DubbingState {
    NEW = 'NEW',
    IN_PROCESS = 'IN_PROCESS',
    UNDER_REVIEW = 'UNDER_REVIEW',
    VERIFIED = 'VERIFIED',
    REJECTED = 'REJECTED',
    EXPIRED = 'EXPIRED',
    CANCELED = 'CANCELED',
    RESTART_REQUESTED = 'RESTART_REQUESTED'
}

export default DubbingState;